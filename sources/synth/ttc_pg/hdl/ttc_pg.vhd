library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--! xilinx packages
library unisim;
use unisim.vcomponents.all;
--! system packages

use work.ttc_pg_pkg.all;
use work.ttc_codec_pkg.all;

entity ttc_pg is 
port
(
  rst_i              : in  std_logic;   
  clk160_i           : in  std_logic;
  clken_i            : in  std_logic;
  pg_ctrl_i          : in  ttc_pg_ctrl_type;
  pg_stat_o          : out ttc_pg_stat_type;  
  pg_o               : out ttc_info_type
);
end ttc_pg;

-----------------------------------------
----- pattern definition ----------------
-----------------------------------------
--
-- [15:0]  -> gap between events
-- [23:16] -> brc[7:0] (ttddddeb)
-- [24]    -> brc strobe
-- [25]    -> l1a                    
--
-- example pattern: 
-- 1st l1a 32 ticks after the bunch counter reset
-- 2nd l1a 16 ticks after the 1st l1a
--
-- [000]: 0x0200001f  (gap=31)
-- [001]: 0x0200000f  (gap=15)
-- [002]: 0x00000000  (end of pattern)
--
-----------------------------------------
-- chA-to-chB delay
-----------------------------------------
-- Note that there is a chA-to-chB delay of 20 ticks due 
-- to the encoding-decoding of the broadcast commands 
-- example: if in the pattern is defined in order to  
-- have the 1st l1a to arrive 32 ticks after the bcnt reset, 
-- at the receiver end we will see the 1st l1a to appear 12 
-- (32-20) ticks after the bunch counter reset, due to the 
-- delayed arrival of the bunch counter reset.
-----------------------------------------





      
architecture top of ttc_pg is                               
  
  signal pg_data     : std_logic_vector(31 downto 0);
  signal pg_pattern  : std_logic_vector( 9 downto 0);
  signal pg_timecnt  : unsigned        (15 downto 0);
    
  signal pg_st       : std_logic_vector( 2 downto 0);
  signal pg_addr     : std_logic_vector( 9 downto 0);
  signal pg_addrcnt  : unsigned        ( 9 downto 0);
  
  signal pg_bcnt     : unsigned(11 downto 0);
  signal bcntrst_r   : std_logic;
  
begin
    
--=====================================--
ttc_pg: process (rst_i, clk160_i) is  -- simple mon/clr/sync generation
--=====================================--

  variable bcntrst     : std_logic;

begin

if rst_i='1' then 
  
  pg_st          <= "000";
  pg_addrcnt     <= (others => '0'); 
  pg_pattern     <= (others => '0'); 
  pg_timecnt     <= (others => '0');
  pg_stat_o.busy <= '0'; 
  pg_stat_o.eop  <= '0';
  pg_bcnt        <= x"000";
  bcntrst        := '0';
  bcntrst_r      <= '0';
elsif rising_edge(clk160_i) then

    bcntrst_r <= bcntrst;
    if pg_ctrl_i.en = '0' then pg_bcnt <= x"deb";    bcntrst := '0';
    elsif clken_i='1'     then
      if pg_bcnt=3564     then pg_bcnt <= x"001";    bcntrst := '1';   
      else                     pg_bcnt <= pg_bcnt+1; bcntrst := '0';
      end if;
    end if; 

    
    if pg_ctrl_i.en='1' then
      case pg_st is
        
        when "000"  => pg_pattern <= (others => '0'); 
                       pg_addrcnt <= (others => '0'); 
                       if bcntrst='1' and bcntrst_r='0' then -- sync with bcntrst
                         pg_st          <= "010";
                         pg_stat_o.busy <= '1'; 
                       end if;   
        
        when "001"  => null; 
        
        when "010"  => --pg_pattern <= x"00";
                       if pg_data/=x"00000000" then 
                         pg_timecnt <= unsigned(pg_data(15 downto 0));
                         pg_st      <= "011";
                       else
                         pg_stat_o.eop <= '1'; -- end of first pattern, exits unless cyclic
                         pg_addrcnt    <= (others => '0'); 
                         if pg_ctrl_i.cyclic ='0' then pg_st <= "110"; else pg_st <= "000"; end if;
                       end if;  
      
        when "011"  => if clken_i = '1' then
                         if pg_timecnt=0 then
                           pg_pattern <= pg_data(25 downto 16);
                           pg_st      <= "100"; 
                           pg_addrcnt <= pg_addrcnt+1;
                         else
                           pg_pattern <= (others => '0');
                           pg_timecnt <= pg_timecnt-1;
                         end if;
                       end if;   
        
        when "100"  => pg_st   <= "101";
        when "101"  => pg_st   <= "010";
        when "110"  => pg_pattern     <= (others => '0');
                       pg_stat_o.busy <= '0';  
                       pg_stat_o.eop  <= '0';
                
        when others => null;
      end case;    
      if bcntrst='1' then pg_pattern <= "0000000001"; end if; 
    else
      pg_st          <= "000";
      pg_addrcnt     <= (others => '0'); 
      pg_pattern     <= (others => '0'); 
      pg_stat_o.busy <= '0';  
      pg_stat_o.eop  <= '0';
    end if;
         
end if;
end process;
--=====================================--

pg_addr          <= std_logic_vector(pg_addrcnt);

pg_o.brc_b       <= pg_pattern(0);
pg_o.brc_e       <= pg_pattern(1);
pg_o.brc_d4      <= pg_pattern(5 downto 2);
pg_o.brc_t2      <= pg_pattern(7 downto 6);
pg_o.brc_strobe  <= pg_pattern(8);
pg_o.l1accept    <= pg_pattern(9);  

pg_o.adr_strobe  <= '0';
pg_o.adr_a14     <= (others => '0');
pg_o.adr_e       <= '0';
pg_o.adr_s8      <= (others => '0');
pg_o.adr_d8      <= (others => '0');

pg_o.clk40_gated <= '0';

--=====================================--
bram: entity work.ttc_pg_bram          -- 1K x 32b
--=====================================--
port map 
(
    clka   => pg_ctrl_i.clk,
    ena    => '1',
    wea(0) => pg_ctrl_i.we,
    addra  => pg_ctrl_i.waddr,
    dina   => pg_ctrl_i.wdata,
    douta  => pg_stat_o.rdata,
    clkb   => clk160_i,
    enb    => '1',
    web(0) => '0',
    addrb  => pg_addr,
    doutb  => pg_data,
    dinb   => (others => '0')
);
--=====================================--


end top;