

library ieee;
use ieee.std_logic_1164.all;

use work.ttc_codec_pkg.all;

entity ttc_encoder is
port 
(
  reset_i     : in  std_logic;
  clk160_i    : in  std_logic;
  clken_i     : in  std_logic;    
  info_i      : in  ttc_info_type;
  data_o      : out std_logic 
);
end ttc_encoder;

architecture rtl of ttc_encoder is
  
  signal brc_str            : std_logic;
  signal brc_rawdata_frame  : std_logic_vector( 7 downto 0); --   ttdddddeb 
  signal brc_encoded_frame  : std_logic_vector(12 downto 0); --   ttdddddebhhhhh
  signal brc_complete_frame : std_logic_vector(15 downto 0); -- 00ttdddddebhhhhh1
  signal brc_padded_frame   : std_logic_vector(41 downto 0); -- 00ttdddddebhhhhh10000000000000000000000000

  signal adr_str            : std_logic;
  signal adr_rawdata_frame  : std_logic_vector(31 downto 0); --   aaaaaaaaaaaaaae1ssssssssdddddddd  
  signal adr_encoded_frame  : std_logic_vector(38 downto 0); --   aaaaaaaaaaaaaae1ssssssssddddddddhhhhhhh
  signal adr_complete_frame : std_logic_vector(41 downto 0); -- 01aaaaaaaaaaaaaae1ssssssssddddddddhhhhhhh1
  
  signal channel_a          : std_logic;
  signal channel_b          : std_logic;
  
  signal chb_fifo_wclk      : std_logic;
  signal chb_fifo_wen       : std_logic;
  signal chb_fifo_wdata     : std_logic_vector(41 downto 0);
  signal chb_fifo_rclk      : std_logic;
  signal chb_fifo_rack      : std_logic;
  signal chb_fifo_rvalid    : std_logic;
  signal chb_fifo_rdata     : std_logic_vector(41 downto 0);
  signal chb_fifo_empty     : std_logic;
  
  signal chb_state          : integer range 0 to 1;  
--signal chb_sr             : std_logic_vector(41 downto 0); 
  signal chb_cnt            : integer range 0 to 41;
  signal enc_state          : integer range 0 to 3;  
  signal enc_bit            : std_logic;
  
  signal long_cmd           : std_logic;        

begin 



  
  
  
  --================================--
  -- brc frame preparation
  --================================--
  brc_str           <= info_i.brc_strobe or info_i.brc_e or info_i.brc_b;    -- a '1' either in brc_e, brc_b or brc_strobe triggers a broadcast command 
  brc_rawdata_frame <= info_i.brc_t2 & info_i.brc_d4 & info_i.brc_e & info_i.brc_b; -- ttddddeb
  
  hmg_8_13: entity work.hamming_8_13 
  port map 
  (
    frame_i => brc_rawdata_frame, -- ttddddeb
    frame_o => brc_encoded_frame  -- ttdddddebhhhhh
  );   

  brc_complete_frame <= "00" & brc_encoded_frame & '1'; -- frame finalize (add frame boundaries)--  
  brc_padded_frame   <= brc_complete_frame & x"000000" & "00";
  --================================--
 

  --================================--
  -- adr frame preparation
  --================================--
  adr_str           <= info_i.adr_strobe;  
  adr_rawdata_frame <= info_i.adr_a14 & info_i.adr_e & '1' & info_i.adr_s8 & info_i.adr_d8; -- aaaaaaaaaaaaaae1ssssssssdddddddd
  
  hmg_32_39: entity work.hamming_32_39 
  port map 
  (
    frame_i => adr_rawdata_frame, -- aaaaaaaaaaaaaae1ssssssssdddddddd
    frame_o => adr_encoded_frame  -- aaaaaaaaaaaaaae1ssssssssddddddddhhhhhhh
  );   

  adr_complete_frame <= "01" & adr_encoded_frame & '1'; -- frame finalize (add frame boundaries)--  
  --================================--
  
  --================================--
  -- dual clock fifo (wrclk -> effective clk40, rdclk -> clk160) 
  --================================--
  chb_fifo_wclk  <= clk160_i;
  chb_fifo_wen   <= (brc_str or adr_str) and clken_i;
  chb_fifo_wdata <= brc_padded_frame   when brc_str='1' 
               else adr_complete_frame when adr_str='1'
               else (others => '0');

  fifo_inst: entity work.chb_fifo
  port map 
  (
    rst          => reset_i,
    wr_clk       => chb_fifo_wclk,
    din          => chb_fifo_wdata,
    wr_en        => chb_fifo_wen,
    full         => open,
    rd_clk       => chb_fifo_rclk,
    rd_en        => chb_fifo_rack, -- fwft implementation
    dout         => chb_fifo_rdata,
    empty        => chb_fifo_empty,
    valid        => chb_fifo_rvalid
  );

  chb_fifo_rclk  <= clk160_i;
  --================================--
      
  
  --================================--
  enc_channels: process (reset_i, clk160_i)
  --================================--
    variable sr : std_logic_vector(41 downto 0);
  begin   
    
  if reset_i = '1' then
    
    chb_fifo_rack <= '0'; 
    chb_state     <=  0 ;
    sr            := (others => '1');
    channel_a     <= '0';
    channel_b     <= '1';
    long_cmd      <= '0';
    
  elsif rising_edge(clk160_i) then 
    if clken_i='1' then
      case chb_state is
        when 0 => 
          sr := (others => '1');
          if chb_fifo_empty='0' then 
            chb_fifo_rack <= '1'; 
            sr := chb_fifo_rdata; long_cmd <= chb_fifo_rdata(40);
            chb_state <= 1 ; 
            chb_cnt  <= 1 ; 
          end if;  
        when 1 => 
          sr := sr(40 downto 0) & '0';
          if (chb_cnt=15 and long_cmd='0') or (chb_cnt=41 and long_cmd='1') then 
            chb_state <= 0 ; 
            chb_cnt   <= 1 ;
          else 
            chb_cnt <= chb_cnt + 1; 
          end if;
      end case;
      
      channel_b <= sr(41);
      channel_a <= info_i.l1accept;
    
    else
      chb_fifo_rack <= '0';   
    end if;   
  end if;  
  end process;   
  --================================--
    
  
  --================================--
  biphase_mark_enc: process (reset_i, clk160_i)
  --================================--
  begin
    if reset_i = '1' then 
      data_o    <='0';
      enc_state <= 0 ;
      enc_bit   <='0';
  elsif rising_edge(clk160_i) then 
    case enc_state is
      when 0 => enc_state <= 1; enc_bit <= not enc_bit;           -- invert always
      when 1 => enc_state <= 2; enc_bit <= enc_bit xor channel_a; -- invert when data = 1 
      when 2 => enc_state <= 3; enc_bit <= not enc_bit;           -- invert always   
      when 3 => enc_state <= 0; enc_bit <= enc_bit xor channel_b; -- invert when data = 1 
    end case;
    data_o   <= enc_bit;
  end if;  
  end process; 
  --================================--
  
  
end rtl;