library ieee;
use ieee.std_logic_1164.all;

use work.ttc_codec_pkg.all;

entity ttc_decoder is
port
(

  rst_i                  : in  std_logic;
  clk160_i               : in  std_logic;
  data_i                 : in  std_logic;
  info_o                 : out ttc_info_type;
  stat_o                 : out ttc_stat_type
);

end ttc_decoder;

architecture top of ttc_decoder is

signal rx_dec_a_channel    : std_logic;
signal rx_dec_b_channel    : std_logic;
signal rx_dec_ab_ready     : std_logic;
signal rx_dec_clken        : std_logic;
signal not_rst             : std_logic;
signal not_rx_dec_ab_ready : std_logic;


--@@@@@@@@@@@@@@@@@@@@@@--
--@@@@@@@@@@@@@@@@@@@@@@--
--@@@@@@@@@@@@@@@@@@@@@@--
begin-- ARCHITECTURE
--@@@@@@@@@@@@@@@@@@@@@@--
--@@@@@@@@@@@@@@@@@@@@@@--
--@@@@@@@@@@@@@@@@@@@@@@--



--==========================--
rx_ab: entity work.cdr2a_b_clk
--==========================--
port map
(
 cdrclk_in       => clk160_i,
 cdrdata_in      => data_i,
 cdrlock_in      => not_rst,
 --
 div_nrst        => stat_o.div_nrst,
 ttc_clock       => info_o.clk40_gated,
 --
 Achannel        => rx_dec_a_channel,
 Bchannel        => rx_dec_b_channel,
 ttc_frame_reset => rx_dec_ab_ready,
 ttc_strobe      => rx_dec_clken
);

not_rst <= not rst_i;
--==========================--






--==========================--
rx_dec_core: entity work.ttc_decoder_core
--==========================--
port map
(
reset_i                => not_rx_dec_ab_ready,
--== ttc cha & b ==--
cha                    => rx_dec_a_channel,
chb                    => rx_dec_b_channel,
clk                    => clk160_i,
clken                  => rx_dec_clken,

-- ttc data out --

l1a                    => info_o.l1accept,

brc_strobe             => info_o.brc_strobe,
brc_t2                 => info_o.brc_t2,
brc_d4                 => info_o.brc_d4,
brc_e                  => info_o.brc_e,
brc_b                  => info_o.brc_b,

add_strobe             => info_o.adr_strobe,
add_a14                => info_o.adr_a14,
add_e                  => info_o.adr_e,
add_s8                 => info_o.adr_s8,
add_d8                 => info_o.adr_d8,

ready                  => stat_o.ready,
single_bit_error       => stat_o.err_sng,
double_bit_error       => stat_o.err_dbl,
communication_error    => stat_o.err_comm

);
not_rx_dec_ab_ready <= not rx_dec_ab_ready;
--==========================--



end top;
