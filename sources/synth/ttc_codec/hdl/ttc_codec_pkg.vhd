library ieee;
use ieee.std_logic_1164.all;
 
package ttc_codec_pkg is
   
  
  
  type ttc_info_type is
  record
  l1accept      : std_logic;  
  brc_strobe    : std_logic;
  brc_t2        : std_logic_vector( 1 downto 0);
  brc_d4        : std_logic_vector( 3 downto 0);
  brc_e         : std_logic;
  brc_b         : std_logic;
  adr_strobe    : std_logic;
  adr_a14       : std_logic_vector(13 downto 0);
  adr_e         : std_logic;
  adr_s8        : std_logic_vector(7 downto 0);
  adr_d8        : std_logic_vector(7 downto 0);
  clk40_gated   : std_logic;
  end record;

  type ttc_stat_type is
  record
  ready         : std_logic;
  err_sng       : std_logic;
  err_dbl       : std_logic;
  err_comm      : std_logic;
  div_nrst      : std_logic;-- required for aligning the 40MHz clock to the input stream
  end record;  
  
    
  constant ttc_info_null : ttc_info_type:=
  (
  l1accept      => '0',  
  brc_strobe    => '0', 
  brc_t2        => "00",
  brc_d4        => "0000",
  brc_e         => '0', 
  brc_b         => '0',
  adr_strobe    => '0',
  adr_a14       => (others => '0'),
  adr_e         => '0',
  adr_s8        => (others => '0'),
  adr_d8        => (others => '0'),
  clk40_gated   => '0'          
  );

  constant ttc_stat_null : ttc_stat_type:=
  (
  ready         => '0',
  err_sng       => '0',  
  err_dbl       => '0', 
  err_comm      => '0',
  div_nrst      => '0'              
  );  
  
end ttc_codec_pkg;
   
package body ttc_codec_pkg is
end ttc_codec_pkg;
