library ieee;
use ieee.std_logic_1164.all;

entity clken_gen is
port 
(
  reset_i     : in  std_logic;
  clk40_i     : in  std_logic:='0'; -- unused for async architecture
  clk160_i    : in  std_logic;
  clken_o     : out std_logic 
);
end clken_gen;


architecture async of clken_gen is

  signal clken_sr        : std_logic_vector(3 downto 0);    
   
begin 

--=====================================--
clken: process(reset_i, clk160_i) 
--=====================================--
begin
if reset_i='1' then
  clken_o         <= '0';
  clken_sr        <= "0001";  
elsif rising_edge(clk160_i) then
  clken_o  <= clken_sr(0);
  clken_sr <= clken_sr(2 downto 0) & clken_sr(3);
end if;
end process;
--=====================================--

end async;

--=====================================--
--#####################################--
--#####################################--
--#####################################--
--#####################################--
--#####################################--
--#####################################--
--#####################################--
--#####################################--
--#####################################--
--#####################################--
--#####################################--
--=====================================--

architecture sync of clken_gen is

  signal clk40_div2      : std_logic;
  signal clken_sr        : std_logic_vector(3 downto 0);    
  signal clk40_div2_prev : std_logic;
  signal clk40_div2_curr : std_logic;
  signal clken_started   : std_logic;
   
begin 

  
--=====================================--
div2: process(reset_i, clk40_i)
--=====================================--
begin
if reset_i='1'             then  clk40_div2 <= '0'; 
elsif rising_edge(clk40_i) then  clk40_div2 <= not clk40_div2;
end if;
end process;
--=====================================--



--=====================================--
clken: process(reset_i, clk160_i) 
--=====================================--
begin
if reset_i='1' then
  
  clken_o         <= '0';
  clk40_div2_prev <= '0';
  clk40_div2_curr <= '0';
  clken_sr        <= "0000";  
  clken_started   <= '0';

elsif rising_edge(clk160_i) then
  
  clken_o  <= clken_sr(0);
  
  clken_sr <= clken_sr(2 downto 0) & clken_sr(3);
  
  if clk40_div2_prev='0' and clk40_div2_curr='1' then 
    clken_sr  <= "0010"; 
  end if;

  if clken_sr(0) ='1' then clken_started <= '1'; end if;
  
  clk40_div2_prev   <= clk40_div2_curr;
  clk40_div2_curr   <= clk40_div2;

end if;
end process;
--=====================================--



end sync;