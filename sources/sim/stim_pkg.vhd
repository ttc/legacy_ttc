library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_textio.all;
use ieee.numeric_std.all;

library std;
use std.textio.all;

package stim_pkg is

  type stim_t is array(natural range<>) of std_logic_vector(63 downto 0);
  
  type size_t is array(natural range<>) of integer;

  procedure clk_gen_t    (signal clk : out std_logic; constant PERIOD : time; constant PHASE  : time );
  procedure clk_gen_f    (signal clk : out std_logic; constant FREQ   : real; constant OFFSET : real ) ;
  procedure clk_gen_t_dc (signal clk : out std_logic; constant PERIOD : time; constant PHASE  : time; constant HIGH_TIME: time; constant HIGH_FIRST: std_logic );
    
end stim_pkg;

package body stim_pkg is

  procedure clk_gen_t
  ( 
    signal   clk : out std_logic; 
    constant PERIOD : time;
    constant PHASE  : time
  ) is

    constant HIGH_TIME : time := PERIOD / 2;          -- High time
    constant LOW_TIME  : time := PERIOD - HIGH_TIME;  -- Low time; always >= HIGH_TIME
  begin
    clk <= '0';  
    wait for PHASE;
    loop
      clk <= '0';
      wait for LOW_TIME;
      clk <= '1';
      wait for HIGH_TIME;
    end loop;
  end procedure;


  procedure clk_gen_f
  ( 
    signal   clk : out std_logic; 
    constant FREQ : real; 
    constant OFFSET: real
  ) is

  constant PERIOD    : time := 1 sec / FREQ;        -- Full period
  constant PHASE     : time := 1 ns * OFFSET;        -- Full period
  constant HIGH_TIME : time := PERIOD / 2;          -- High time
  constant LOW_TIME  : time := PERIOD - HIGH_TIME;  -- Low time; always >= HIGH_TIME
  begin
    clk <= '0';
    wait for PHASE;
    loop 
      clk <= '0';
      wait for LOW_TIME;
      clk <= '1';
      wait for HIGH_TIME;
    end loop;
  end procedure;

  procedure clk_gen_t_dc
  ( 
    signal   clk        : out std_logic; 
    constant PERIOD     : time;
    constant PHASE      : time;
    constant HIGH_TIME  : time;
    constant HIGH_FIRST : std_logic
  ) is

    constant LOW_TIME  : time := PERIOD - HIGH_TIME;  -- Low time; always >= HIGH_TIME
  begin
    clk <= '0';  
    wait for PHASE;
    loop
      if HIGH_FIRST='1' then
        clk <= '1';
        wait for HIGH_TIME;
        clk <= '0';
        wait for LOW_TIME;
      else
        clk <= '0';
        wait for LOW_TIME;
        clk <= '1';
        wait for HIGH_TIME;
      end if;      
    end loop;
  end procedure;

end stim_pkg;
